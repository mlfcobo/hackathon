from django.urls import path
from . import views

app_name = "bible"

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('feed', views.FeedView.as_view(), name='feed'),
    path('login', views.LogInView.as_view(), name='login'),
    path('signup', views.SignUpView.as_view(), name='signup'),
    path('logout', views.LogOutView.as_view(), name='logout'),
    path('kick/<int:receiver_id>', views.kick, name='kick'),
    path('send/<int:receiver_id>', views.send, name='send'),
]