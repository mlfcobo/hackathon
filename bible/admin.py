from django.contrib import admin

from .models import UserProfile, Verse, Kick

admin.site.register(UserProfile)
admin.site.register(Verse)
admin.site.register(Kick)