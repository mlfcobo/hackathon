from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    coins = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username

class Verse(models.Model):
    content = models.CharField(max_length=2000)
    source = models.CharField(max_length=50)

    def __str__(self):
        return self.source

class Kick(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='receiver')
    verse = models.ForeignKey(Verse, on_delete=models.CASCADE)
