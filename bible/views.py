from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .models import UserProfile, Verse, Kick
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class IndexView(generic.View):
    template_name = 'bible/index.html'

    def get(self, request, *args, **kwargs):
        userCreationForm = UserCreationForm()
        return render(request, self.template_name, {"form": UserCreationForm})


class FeedView(LoginRequiredMixin, generic.View):
    template_name = 'bible/feed.html'

    def get(self, request, *args, **kwargs):
        feed = Kick.objects.filter(receiver=request.user)
        user = User.objects.all().exclude(username=request.user.username)
        context = {
            'feeds': feed,
            'users': user,
        }
        return render(request, self.template_name, context)


class LogInView(generic.View):

    def post(self, request, *args, **kwargs):
        print('SIGNING IN')
        username = request.POST['username']
        password = request.POST['password1']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            return redirect('bible:feed')
        else:
            # Return an 'invalid login' error message
            f = UserCreationForm()
            context = {
                'error_message': "Incorrect username or password.",
                'form': f,
            }
            return render(request, 'bible/index.html', context)


class SignUpView(generic.View):
    def post(self, request, *args, **kwargs):
        print('Signing up')
        username = request.POST['username']
        password = request.POST['password1']
        f = UserCreationForm(request.POST)
        if f.is_valid():
            user = User.objects.create_user(
                username=username, password=password)
            user = authenticate(request, username=username, password=password)
            userProfile = UserProfile(user=user)
            if user is not None:
                userProfile.save()
                user.save()
                login(request, user)
            return redirect('bible:feed')

        else:
            f = UserCreationForm()
            return render(request, 'bible/index.html', {"form": f})


class LogOutView(generic.View):

    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('bible:index')


# class KickView(LoginRequiredMixin, generic.View):
#     template_name = 'bible/kick.html'

#     def post(self, request, *args, **kwargs):
#         receiver_pk = request.POST['pk']
#         verse_id = request.POST['verse_id']
#         verse = Verse.objects.get(pk=verse_id)
#         receiver = User.objects.get(pk=pk)
#         kick = Kick(sender=request.user, receiver=receiver, verse=verse)
#         kick.save()
#         return redirect('bible:feed')

def kick(request, receiver_id):
    verse = Verse.objects.all()
    receiver = User.objects.get(pk=receiver_id)
    context = {
        'verses': verse,
        'receiver': receiver,
    }
    return render(request, 'bible/kick.html', context)

def send(request, receiver_id):
    if(request.user.userprofile.coins < 1):
        feed = Kick.objects.filter(receiver=request.user)
        user = User.objects.all().exclude(username=request.user.username)
        context = {
            'feeds': feed,
            'users': user,
            'low_coins' :"Sorry, you have insufficient coins.",
        }
        return render(request,'bible/feed.html',context)
    else:
        request.user.userprofile.coins -= 1
        request.user.userprofile.save()
        receiver = User.objects.get(pk=receiver_id)
        receiver.userprofile.coins +=  5
        receiver.userprofile.save()
        verse = Verse.objects.get(pk=request.POST['verse'])
        kick = Kick(sender=request.user, receiver=receiver, verse=verse)
        kick.save()
        return redirect('bible:feed')