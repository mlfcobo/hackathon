                                 XXXXX
                                 XXXXX
                                 XXXXX
                                 XXXXX
                          XXXXXXX.-.-.XXXXXXX     I asked Jesus, "How much do you love me?"
                          XXXXXXX`. .'XXXXXXX     Jesus answered, "This much." as He stretched
                          XXXXXXXX ` XXXXXXXX     His arms and died on the cross for me. (And
                                 XXXXX            you too!)
                                 XXXXX
                                 XXXXX
                                 XXXXX
                                 XXXXX
                                 XXXXX
                                 XXXXX
                                 XXXXX


***
# The App
>Spread the word of God through a fun and exciting way charot juklang. Be the next gen disciple of God *insert angel emoji*

***
# How the app works
+ **Create** an account
+ **Login** and see the list of people who are also using the app
+ **Kick** (pass) a bible verse to another person (a kick costs 1 coin and the receiver of your kick receives 5 points)
+ **Feed** contains all bible verses your acquaintances shared to you

***
>python=3.5.2
>
>django=2.0.6

***
#### Members:
+ Mark Cobo
+ Brandon Flores
+ Lance Labrague
+ Noah Silvio

##### Bible, kick, acquaintance, transaction


